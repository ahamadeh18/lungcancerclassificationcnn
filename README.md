# LungCancerClassificationCNN



## I. Introduction:

This project is a Convolutional Neural Network that classifies malignant and healthy lung tissue. This model is able to detect 3 types of tumors in the lungs which are: 1) Adenocarcinoma, 2) Large cell carcinoma, and 3) Squamous cell carcinoma.

This model can detect the following kind of tumors with an overall accuracy of 99.47% on the test set, 98.01% on the train set, and 92.02% on the validation set and class accuracies as follows:
1. Healthy or normal tissue: 98.5%
2. Adenocarcinoma: 100%
3. Large cell carcinoma: 100%
4. Squamous cell carcinoma: 100%

## II. Model Architecture:

This model consists of 3 convolutional layers and a fully-connected layer. The details of the model are as follows:

### 1. First Layer:
* Convolution operation with a kernel size of 3 by 3, a padding of 1, and a stride of 1.
* Batch normalization.
* Followed by max-pooling of kernel size of 3 by 3, and a stride of 3.
* Leaky ReLU activation with a negative slope of 0.1
* Dropout with a probability of 50%
This layer has 32 filters.

### 2. Second Layer:
* Convolution operation with a kernel size of 3 by 3, a padding of 1, and a stride of 1.
* Batch normalization.
* Followed by max-pooling of kernel size of 3 by 3, and a stride of 3.
* Leaky ReLU activation with a negative slope of 0.1
* Dropout with a probability of 50%
This layer has 64 filters.

### 3. Third Layer:
* Convolution operation with a kernel size of 3 by 3, a padding of 1, and a stride of 1.
* Batch normalization.
* Followed by max-pooling of kernel size of 3 by 3, and a stride of 3.
* Leaky ReLU activation with a negative slope of 0.1
* Dropout with a probability of 50%
This layer also has 64 filters.

### 4. The Fully-connected Layer:
In this layer, the output of the previous convolutional layer is flattened into a vector and is then fed to a linear layer. This layer has 4 outputs that correspond to all of the classes.

## NOTE: 
For the convolutional layers, all of the weights are initialized using the kaiming method and with biases equal to zero.

## III. Dataset:
This model was trained on the [Chest CT-Scan images Dataset](https://www.kaggle.com/datasets/mohamedhanyyy/chest-ctscan-images) from Kaggle.
### Dataset details:
This dataset consists of 1000 CT-scan chest images. The dataset was split into train, validation, and test sets.

### Challenges with the dataset:
The dataset was already split into training, validation, and test set. However, after training with the data it seems that those splits were causing the model to show overfitting behavior since the distribution of the images in the train split was somewhat different from that of the validation and the test splits. Another thing to mention is that the splits were as follows: 613 for the train set, 72 for the validation set, and 315 test. It is evident that the validation set is relatively small compared to the other splits which could cause the model to show misleading results. Another problem with the data is class imbalance (338 images for the adenocarcinoma class, 187 images for the large cell carcinoma class, 169 images for the normal tissue class, and 252 images for the squamous cell carcinoma class)
### Solutions:
As a solution to this problem, all of the splits were joined together and split programmatically in Python. as for the class imbalance issue, a weighted loss function was used during training.

## IV. Training the model:
Training this model consists of x steps:
### 1. Preprocessing the data:
The data was pre-processed before training. The pre-processing consists of:
1. Sharpening the images.
2. Normalizing the dataset by finding the mean and standard deviation of the dataset and then subtracting the mean and dividing by the standard deviation.
3. Resizing the images to 600 by 800. These dimensions were obtained experimentally.
### 2. Data augmentation:
Due to the training set being small, data augmentation was needed in order not to overfit the model.
The data augmentation was done by sampling images from the train split and applying some transformations to them.
The transformations were:
1. Random horizontal flipping with a flipping probability of 50%
2. Random sharpness adjustment with a probability of 50%
3. Finally, Random auto contrast also with a probability of 50%
For training 5000 images were augmented (This value was obtained experimentally)
### 3. Setting up the training routine:
The training routine consisted of the following:
### A. Data loaders:
Three data loader (torch.utils.data.DataLoader) objects were created for each data split (train, validation, and test) with a number of workers of 4 and a batch size of 16.
### B. The Optimizer:
The optimizer is Stochastic Gradient Descent with:
* Initial learning rate of 1e-3
* Weight decay of 1e-5
* Momentum of 0.9
This optimizer was accompanied by a learning rate scheduler which is StepLR with a step size of 23 and a gamma value of 0.1
### C. The Loss Function:
The training set had a class imbalance issue. To tackle this weighted cross entropy loss function was used.
### D. Epochs:
The model was trained for 30 epochs.

## V. Results:
After training for 30 epochs the model achieved train accuracy of 98.01%, validation accuracy of 92.02% and a test accuracy of 99.47%.

# IMPROTANT NOTE: 
* It is important to include a .txt file in the data folder with the name "paths.txt" which contains the path to the data.
The file format should be as follows:
<pre><code>{
    "data_path" : "[The path of the data]"
}
</pre></code>
* The modified dataset can be found [here](https://drive.google.com/drive/folders/1RotOe6dVwPNcxlC1vfCAciUs0b3oA9dH?usp=sharing)
* For figures of the train, validation accuracies and the loss behavior refer to the figures in the main_notebook.ipynb file.

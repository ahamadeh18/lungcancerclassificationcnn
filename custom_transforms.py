# This file contains some potentially useful
# custom tranforms.

from torchvision import transforms as T
import cv2

class Sharpen(object):
    """ This class will be used as a transform in the data
    pre-processing. This transform sharpens the given image
    by a given factor.
    """
    def __init__(self, sharpness_factor):
        self.sharpness_factor = sharpness_factor
        
    def __call__(self, image):
        sharpened_image = T.functional.adjust_sharpness(image, self.sharpness_factor)
        return sharpened_image
    

class Blur(object):
    """ This class will be used as a transform in the data
    pre-processing. This transform applies gaussian blur
    filter to the given image with a given filter kernel
    size.
    """
    def __init__(self, kernel_size):
        
        self.kernel_size = kernel_size
    
    def __call__(self, image):
        denoised_image = T.functional.gaussian_blur(image, self.kernel_size)
        return denoised_image
                           
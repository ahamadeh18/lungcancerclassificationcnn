### File description:
#       This file contains the Dataset class that loads
#       the Lung Cancer dataset.

# PyTorch imports:
import torch
from torch.utils.data import Dataset
from torchvision import transforms as T

# Numpy imports:
import numpy as np

# OpenCV imports:
import cv2

# General imports:
import json
import os

# The path of the txt file that contains the path for the dataset:
PATHS_FILE = os.path.join(os.path.dirname(os.path.abspath(__file__)), "paths.txt")


class LungCancerDataset(Dataset):
    def __init__(self, data_path=None, transform=None, augment=False,
                 augment_amount=1000, augment_transform=None, indecies=None):
        """ The costructor to the LungCancerDataset object.

        Args:
            data_path (str, optional): The path to the data this dataset object
            loads. If left None the data will be loaded from the PATHS_FILE 
            variable. Defaults to None.
            transform (torchvision.transforms, optional): The transform to be
            applied to the images as pre-processing. Defaults to None.
            augment (bool, optional): If true, augmentation will be applied
            and the number of data points increases. Defaults to False.
            augment_amount (int, optional): The amount of data points to 
            augment. Defaults to 1000.
            augment_transform (torchvision.transforms, optional): The augmentation
            criterion. Defaults to None.
            indecies (np.ndarray, optional): The indecies that specify which 
            data points to sample from the whole data (This is useful when
            splitting the data). Defaults to None.
        """
        self.data_path = data_path
        self.transform = transform
        
        self.indecies = indecies
        
        if data_path is None:
            # If the data path is not provided, it is loaded from the
            # paths.txt file.
            with open(PATHS_FILE, 'r') as path_file:
                dataset_path = json.loads(path_file.read())
                dataset_path = dataset_path['data_path']
            
            self.data_path = dataset_path
        
        # Loading the images and their labels:
        self.image_paths, self.labels = self.load_image_paths()

        self.classes = np.unique(self.labels)
        
        # Encoding the labels to numbers so they can be used
        # in the neural network later on:
        self.encoded_labels, self.encoding = self.encode_labels()
        
        self.augment = augment
        self.augment_amount = augment_amount
        self.augmentation_transform = augment_transform
        
        # Augment the data if the augment flag is True:
        if self.augment:
            if augment_transform is None:
                
                # The default augmentation transforms. This will be applied if the 
                # augment flag is set to True.
                self.augmentation_transform = T.Compose([T.ToTensor(),
                                                T.RandomHorizontalFlip(p=0.5),
                                                T.RandomAdjustSharpness(sharpness_factor=2.0, p=0.5),
                                                T.RandomAutocontrast(p=0.5)])
            
            self.augment_data()
            
        
    def load_image_paths(self):
        """ Loads the paths of the images from the path specified
        in the __init__, namely, the self.data_path.

        Returns:
            np.ndarray: A numpy array containing the paths to all
                        the images in the dataset.
            np.ndarray: A numpy array containing the labels to 
                        the images in the dataset.
        """
        classes = os.listdir(self.data_path)
        
        image_paths = np.array([])
        labels = np.array([])
        
        for cls in classes:
            
            current_path = self.data_path
            current_path = os.path.join(current_path, cls)
            
            for image_path in os.listdir(current_path):
                image_path = os.path.join(current_path, image_path)
                
                image_paths = np.append(image_paths, image_path)
                labels = np.append(labels, cls)
        
        if self.indecies is not None:
            image_paths = np.copy(image_paths[self.indecies])
            labels = np.copy(labels[self.indecies])
         
        return image_paths, labels
    
    def encode_labels(self):
        """ Maps the original (str) labels to numbers.

        Returns:
            np.ndarray: A numpy array containing the mapped labels
            dict: A dictionary containing the mapping criterion
        """
        # Creating the encoding criterion which is mapping the str labels
        # to numbers from 0 to the number of classes - 1
        mapping = dict(zip(self.classes, list(range(0, len(self.classes)))))

        # Encoding the labels according to the 'mapping' dict:
        encoded_labels = np.array([mapping[label] for label in self.labels])
        
        return encoded_labels, mapping
        
        
    def data_distribution(self):
        """ Finds the number of data points for each class
        in the dataset.

        Returns:
            dict: A dictionary with keys being the names of classes
                and values being the number of data points from
                that class.
        """
        # Finding the number of data points for each class:
        bins = np.bincount(self.encoded_labels)
        # Creating the dict that contains the class names and 
        # number of data points in each class:
        data_dist = dict(zip(self.classes, bins))
        
        return data_dist
    
    
    def augment_data(self):
        """ Augments the data by adding augment_amount of images paths to
        the self.image_paths instance variable and the corresponding labels 
        to the self.encoded_labels and self.labels variables.
        """
        
        # Getting augment_amount of indecies so that the image paths that
        # correspond to those indecies will be added to the self.image_paths
        # along with their labels that will be added to the self.labels and
        # self.encoded_labels variables.
        augment_indecies = np.random.choice(list(range(0, len(self))),
                                            self.augment_amount, replace=True)
        
        # Adding the new image paths:
        self.image_paths = np.append(self.image_paths, self.image_paths[augment_indecies])
        # Adding the encoded labels:
        self.encoded_labels = np.append(self.encoded_labels, self.encoded_labels[augment_indecies])
        # Adding the original labels:
        self.labels = np.append(self.labels, self.labels[augment_indecies])     
        
        
    def class_weights(self):
        """ Returns a list containing the weights of each class.
        Returns:
            list: a list containing the weights of each class.
        """
        class_dist = self.data_distribution()
        
        # Given a class number of data points, returns the weight of this class:
        class_weight = lambda x : (1 / x) * len(self) / len(np.unique(self.labels))
        
        weights = []
        for value in list(class_dist.values()):
            weight = class_weight(value)
            weights.append(weight)
        
        return weights
    
    def __getitem__(self, index : int):
        """ Loads the an image along with its label and encoded
        label given the image's index.  

        Args:
            index (int): The index of the image to be returned

        Returns:
            N/A: A np.ndarray representing an image. Note: if the
            The type of the image could vary according to self.transform
            variable since self.transform could include a type transform
            such as ToTensor().
            int: The encoded label of the image.
            str: The original label of the image.
        """
        image_path = self.image_paths[index]
        # Loading the image from the corresponding image path:
        image = cv2.imread(image_path)
        
        if self.augment:
            # Apply the augmentation with following probability: (This probability
            # is the ratio of the augmented data and the whole number of data points)
            apply_probability = self.augment_amount / (len(self) + self.augment_amount)
            p = np.random.random()
            
            if p < apply_probability:
                image = self.augmentation_transform(image).cpu().detach().numpy()
                image = np.transpose(image, [1, 2, 0])
        # Applying the transform to the image:
        if self.transform is not None:
            image = self.transform(image)
        
        mapped_label = self.encoded_labels[index]
        label = self.labels[index]
        
        return image, mapped_label.astype(np.int64), label
    
        
    def __len__(self):
        return self.labels.shape[0]
    
    

        





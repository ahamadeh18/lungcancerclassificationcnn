import torch


def get_device():
    """ Returns 'cpu' if there is no cuda device on
    the machine running this code.

    Returns:
        str: The device that the model will be evaluated on.
    """
    device = 'cpu'
    if torch.cuda.is_available():
        device = 'cuda'
        
    return device


def evaluate(model, data_loader):
    """ Evaluates a given model using a data loader. Evaluates
    the model according to the accuracy metric.

    Args:
        model (torch.nn.Module): The model that is to be evaluated
        data_loader (torch.utils.data.DataLoader): The data loader that contains
        the data the model will be evaluated with.      

    Returns:
        float: The accuracy of the model.
    """
    # Getting the device where the model evaluation will take place:
    device = get_device()
    
    # Setting the model to evaluation mode:
    model.eval()
    # Moving the model to the device:
    model.to(device=device)
    
    num_correct = 0
    count = 0
    for x, y, _ in data_loader:
        
        x = x.to(device=device)
        y = y.to(device=device)
        
        y_pred = model(x)
        # Getting the predicted class for each image in the batch:
        _, y_pred = torch.max(y_pred, axis=1)
        # Finding how many labels were predicted correctly:
        num_correct += (y_pred == y).sum().item()
        # Using count variable becuase len(data_loader) * data_loader.batch_size does not
        # necessarly return the correct number of data points. Consider the case where
        # the number of data points is not divisible by the batch size.
        # y_pred.shape[0] Will give the number of images in a batch. This number will be
        # equal to the data_loader.batch_size for the most part, however, it will be 
        # different in the case where the number of images is not divisible by the batch
        # size. The last batch will have a number of images equal to the remainder of this
        # division operation.
        count += y_pred.shape[0]
    
    accuracy = 100 * num_correct / count
    
    return accuracy
        
        
        
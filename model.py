### This file contains the CNN model.
#
# The model's architecture is as follow:
#                                                                           
#        _______                    _______                    _______    
#       |    ___|___               |    ___|___               |    ___|___                [*]
#       |   |       | == Leaky ==> |   |       | == Leaky ==> |   |       | == Leaky ==>  [*]
#       |___| 3 x 3 |    ReLU      |___| 3 x 3 |    ReLU      |___| 3 x 3 |    ReLU       [*]
#           |_______|                  |_______|                  |_______|               [*]
#                                                                        
#         Conv Layer                 Conv Layer                 Conv Layer          Fully-connected      
#         32 Filters                 64 Filters                 64 Filters               Layer
#                                                                                      4 outputs.
#
#   NOTE: After each convolution there is:
#           1) Max pooling with 3 x 3 kernel and a stride of 3.
#           2) Batch normalization.
#           3) Dropout with probability p.
#           
# ----------------------------------------------------------------------------------------------
#

# PyTorch imports:
import torch.nn as nn
import torch


class LungCancerModel(nn.Module):
    def __init__(self, num_classes=4, in_channel=3, image_dim=(600, 800), dropout_prob=0.5):
        super(LungCancerModel, self).__init__()
        
        
        #  ====================================================================================
        # /-- First Conv Layer: --------------------------------------------------------------/
        
        kernel_size, padding, stride = 3, 1, 1
        out_channels_1 = 32
        
        self.conv_1 = nn.Conv2d(in_channels=in_channel, out_channels=out_channels_1,
                               kernel_size=kernel_size, padding=padding, stride=stride)
        
        # --- Changing the feature map size after the 1st Convolution: -----------------------|
        new_feature_map_size = self.new_image_size(image_dim=image_dim, kernel_size=kernel_size,
                                                  padding=padding, stride=stride)
        # -------------------------------------------------------------------------------------
        kernel_size = 3
        self.maxpool_1 = nn.MaxPool2d(kernel_size=kernel_size, stride=kernel_size)
        
        # --- Changing the feature map size after the 1st Max Pool: --------------------------|
        new_feature_map_size = self.new_image_size(image_dim=new_feature_map_size,
                                                   kernel_size=kernel_size,
                                                   padding=0,
                                                   stride=kernel_size)
        # -------------------------------------------------------------------------------------
                
        # --- Defining the batch normalization for the first layer: ---------------------------
        self.batchnorm_1 = nn.BatchNorm2d(num_features=out_channels_1)
        
        #  ====================================================================================
        # /-- Second Conv Layer: -------------------------------------------------------------/
        
        kernel_size, padding, stride = 3, 1, 1
        out_channels_2 = 64
        
        self.conv_2 = nn.Conv2d(in_channels=out_channels_1, out_channels=out_channels_2,
                                kernel_size=kernel_size, padding=padding, stride=stride)

        # --- Changing the feature map size after the 2nd Convolution: ------------------------|
        new_feature_map_size = self.new_image_size(image_dim=new_feature_map_size, kernel_size=kernel_size,
                                                  padding=padding, stride=stride)
        # -------------------------------------------------------------------------------------
        kernel_size = 3
        self.maxpool_2 = nn.MaxPool2d(kernel_size=kernel_size, stride=kernel_size)
        
        # --- Changing the feature map size after the 2nd Max Pool: ---------------------------|
        new_feature_map_size = self.new_image_size(image_dim=new_feature_map_size,
                                                   kernel_size=kernel_size,
                                                   padding=0,
                                                   stride=kernel_size)
        
        
        # --- Defining the batch normalization for the second layer: ---------------------------
        self.batchnorm_2 = nn.BatchNorm2d(num_features=out_channels_2)
        
        #  ====================================================================================
        # /-- Third Conv Layer: --------------------------------------------------------------/
        
        kernel_size, padding, stride = 3, 1, 1
        out_channels_3 = 64
        
        self.conv_3 = nn.Conv2d(in_channels=out_channels_2, out_channels=out_channels_3,
                                kernel_size=kernel_size, padding=padding, stride=stride)
        
        # --- Changing the feature map size after the 3nd Convolution: ------------------------|
        new_feature_map_size = self.new_image_size(image_dim=new_feature_map_size, kernel_size=kernel_size,
                                                  padding=padding, stride=stride)
        # -------------------------------------------------------------------------------------
        kernel_size = 3
        self.maxpool_3 = nn.MaxPool2d(kernel_size=kernel_size, stride=kernel_size)
        
        # --- Changing the feature map size after the 3rd Max Pool: ---------------------------|
        new_feature_map_size = self.new_image_size(image_dim=new_feature_map_size,
                                                   kernel_size=kernel_size,
                                                   padding=0,
                                                   stride=kernel_size)
        
        # --- Defining the batch normalization for the third layer: ---------------------------
        self.batchnorm_3 = nn.BatchNorm2d(num_features=out_channels_3)
        
        #  ====================================================================================
        # /-- The Fully-connected Layer: -----------------------------------------------------/
        
        in_features = out_channels_3 * new_feature_map_size[0] * new_feature_map_size[1]
        
        self.fc = nn.Linear(in_features=in_features, out_features=num_classes)
        
        # --- Defining the dropout: -----------------------------------------------------------
        self.dropout = nn.Dropout(p=dropout_prob)
        
        # --- Defining the activation function ------------------------------------------------
        self.leaky_relu = nn.LeakyReLU(negative_slope=0.1)
        
        #  ====================================================================================
        # /-- Weight Initialization: ---------------------------------------------------------/
        
        nn.init.kaiming_normal_(self.conv_1.weight)
        nn.init.kaiming_normal_(self.conv_2.weight)
        nn.init.kaiming_normal_(self.conv_3.weight)
        
        #  ====================================================================================
        # /-- Bias Initialization: -----------------------------------------------------------/
        
        nn.init.constant_(self.conv_1.bias, 0.0)
        nn.init.constant_(self.conv_2.bias, 0.0)
        nn.init.constant_(self.conv_3.bias, 0.0)
        
        # =====================================================================================
    
    
    def forward(self, x):
        
        # --- 1st Layer -----------------------------------------------------------------------
        x = self.conv_1(x)
        x = self.batchnorm_1(x)
        x = self.maxpool_1(x)
        
        x = self.leaky_relu(x)
        x = self.dropout(x)
        
        # --- 2nd Layer -----------------------------------------------------------------------
        x = self.conv_2(x)
        x = self.batchnorm_2(x)
        x = self.maxpool_2(x)
        
        x = self.leaky_relu(x)
        x = self.dropout(x)
        
        # --- 3rd Layer -----------------------------------------------------------------------
        x = self.conv_3(x)
        x = self.batchnorm_3(x)
        x = self.maxpool_3(x)
        
        x = self.leaky_relu(x)
        x = self.dropout(x)
        
        # --- Fully-connected Layer -----------------------------------------------------------
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        
        return x
    
    def new_image_size(self, image_dim : tuple, kernel_size : int, padding : int, stride : int):
        """ Finds the new dimensions of an input feature map after applying
        convolution or pooling.

        Args:
            image_dim (tuple): A tuple containing the dimension of the input 
            feature map.
            kernel_size (int): The kernel size of the filter.
            padding (int): The amount of padding to the feature map.
            stride (int): The stride (step size) of the filter.
        """
        
        new_image_height = (image_dim[0] + 2 * padding - kernel_size) // stride + 1
        new_image_width = (image_dim[1] + 2 * padding - kernel_size) // stride + 1
        
        return new_image_height, new_image_width
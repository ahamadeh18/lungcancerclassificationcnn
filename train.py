import torch
from evaluate import *
from tqdm import tqdm

def get_device():
    """ Returns 'cpu' if there is no cuda device on
    the machine running this code.

    Returns:
        str: The device that the model will be evaluated on.
    """
    device = 'cpu'
    if torch.cuda.is_available():
        device = 'cuda'
        
    return device



def train(model, train_loader, validation_loader, optimizer,
          criterion, epochs : int, scheduler=None, verbose=False):
    """ The training routine for the model.

    Args:
        model (torch.nn.Module): The model to be trained
        train_loader (torch.utils.data.DataLoader): The train loader that the model
        will be trained on.
        validation_loader (torch.utils.data.DataLoader): The validation loader that
        the model will be evaluated on. 
        optimizer (nn.optim): The optimizer that will update the parameters of the
        model.
        criterion (torch.nn): The loss function.
        epochs (int): The number of epochs the model will be trained for.
        scheduler (nn.optim.lr_scheduler, optional): The scheduler that updates the
        learning rate. Defaults to None.
        verbose (bool, optional): If true, provides information about each epoch.
        Defaults to False.
    """
    # Getting the device the model will be trained on:
    device = get_device()
    

    
    # Moving the model to the device:
    model.to(device=device)
    
    total_loss = []    
    
    train_accuracies = []
    validation_accuracies = []
    
    for epoch in tqdm(range(epochs)):
        epoch_loss = 0
        
        # Setting the model to the training mode:
        model.train()
        
        for x, y, _ in train_loader:
            
            # Moving the image and its label
            # to the device:
            x = x.to(device=device)
            y = y.to(device=device)
            
            # Setting the gradiants to zero so they do not
            # conflict with gradiants from other batches:
            optimizer.zero_grad()
            
            # The forward pass:
            y_pred = model(x)
   
            # Calculating the loss from the forward pass:
            loss = criterion(y_pred, y)
            epoch_loss += loss.item()
            
            # Back propagation:
            loss.backward()
            optimizer.step()
        
        print('Evaluating the current epoch...', flush=True)
        
        total_loss.append(epoch_loss)
        
        # ===========================================================
        # --- Evaluating the model ----------------------------------
        
        # --- With the train set: -----------------------------------
        train_accuracy = evaluate(model, train_loader)
        train_accuracies.append(train_accuracy)
        
        # --- With the validation set: ------------------------------
        validation_accuracy = evaluate(model, validation_loader)
        validation_accuracies.append(validation_accuracy)
        #
        # ===========================================================
        
        # If the there is a scheduler then advance the scheduler and
        # print the current learning rate:
        if scheduler is not None:
            learning_rate = optimizer.param_groups[0]['lr']
            print(f'Current learning rate: {learning_rate}')
            scheduler.step()
        
        # Printing some relevant information about each epoch is the verbose vairable
        # was set to True:
        if verbose:
            print(f'Epoch: {epoch} | Loss: {epoch_loss:.2f} | Train accuracy: {train_accuracy:.2f}% \
| Validation accuracy {validation_accuracy:.2f}%')

    return total_loss, train_accuracies, validation_accuracies
